# Алгоритм сортировки и перестановки чисел.

```
using System;

namespace LW3
{
	class Program
	{
		static void Main(string[] args)
		{
			int k, max;
			Console.Write("Введите n : ");
			try
			{
				int n = Convert.ToInt32(Console.ReadLine());

				while (n > 0)
				{
					Random random = new Random();
					int[] array = new int[n];
					for (int i = 0; i < n; i++)
						array[i] = random.Next(0, 1000);

					Console.WriteLine("Исходный массив:");
					foreach (int x in array)
					{
						Console.Write(x + " ");
					}

					max = 0;

					for (int i = 0; i < array.Length; i++)
					{
						if (array[i] > max)
						{
							max = array[i];
							k = array[0];
							array[i] = k;
							array[0] = max;
						}
					}

					Console.WriteLine();
					Console.WriteLine("Полученный массив:");
					foreach (int i in array)
						Console.Write(i + " ");


					n--;
				}
			}
			catch
			{
				Console.WriteLine("Введите целое число!");
			}
			Console.ReadKey();
		}
	}
}
```

